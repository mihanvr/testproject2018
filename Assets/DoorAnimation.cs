﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorAnimation : ActivableComponent {

    public Animator doorAnimator;

    void SetDoorOpened(bool opened) {
        doorAnimator.SetBool("DoorOpen", opened);
    }

    public override void Switch() {
        var opened = doorAnimator.GetBool("DoorOpen");
        SetDoorOpened(!opened);
    }
}
