﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CharacterController : MonoBehaviour {

    HashSet<ActivableComponent> targets = new HashSet<ActivableComponent>();

    public UnityEngine.UI.Text text;
    public UnityEngine.UI.Button switchButton;


    public void Start() {
        UpdateStates();
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetKeyDown(KeyCode.Q)) {
            SwitchTarget();   
        }

    }

    private void UpdateStates() {
        text.text = string.Join("\n", targets.Select(it => it.name));
        if (targets.Count == 0) {
            switchButton.interactable = false;
        } else {
            switchButton.interactable = true;
        }
    }    

    private void OnTriggerEnter(Collider other) {
        var activable = other.GetComponent<ActivableComponent>();
        if (activable != null) {
            targets.Add(activable);
            UpdateStates();
        }
    }
    private void OnTriggerExit(Collider other) {
        var activable = other.GetComponent<ActivableComponent>();
        if (activable != null) {
            targets.Remove(activable);
            UpdateStates();
        }
    }
    public void SwitchTarget() {
        foreach (var target in targets) {
            target.Switch();
        }
    }
}
