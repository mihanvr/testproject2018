﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorchOnOff : MonoBehaviour {

    public GameObject torchLight;

    public void Switch()
    {
        var currentState = !torchLight.active;
        torchLight.SetActive(currentState);
        PlayerPrefs.SetInt(GetSafeKey(), currentState ? 1 : 0);
    }

    private void Start() {
        bool currentState = PlayerPrefs.GetInt(GetSafeKey(), 0) != 0;
        torchLight.SetActive(currentState);
    }

    private string GetSafeKey()
    {
        return transform.parent.name;
    }

    void OnTriggerEnter(Collider collider) {
        if (collider.tag != "Player") return;
        Switch();
    }
    //private void OnTriggerExit(Collider other) {
    //    if (other.tag != "Player") return;
    //    torchLight.SetActive(false);
    //}

}
