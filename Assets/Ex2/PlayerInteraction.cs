﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteraction : MonoBehaviour {
    private TorchOnOff torchOnOff;
    void OnTriggerEnter(Collider collider) {
        var tmp = collider.GetComponent<TorchOnOff>();
        if (tmp != null) {
            torchOnOff = tmp;
        }
    }
        private void Update() {
        if (Input.GetKeyDown(KeyCode.Q)) {
            if (torchOnOff != null) {
                torchOnOff.Switch();
            }
        }
    }
}
