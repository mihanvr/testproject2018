﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ActivableComponent : MonoBehaviour {

    public abstract void Switch();
}
