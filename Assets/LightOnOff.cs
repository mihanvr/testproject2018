﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightOnOff : ActivableComponent {
    public GameObject fire;
    public bool autoTrigger= false;

    private void Start() {
      int pref=  PlayerPrefs.GetInt(GetKey(), 0);
        fire.SetActive(pref != 0);
    }
    public override void Switch() {
        fire.SetActive(!fire.activeSelf);
        PlayerPrefs.SetInt(GetKey(), fire.activeSelf ? 1 : 0);
    }
    private void OnTriggerEnter(Collider other) {
        if (!autoTrigger) return; 
        if (other.tag == "Player") Switch(); 
    }
    private string GetKey() {
        return transform.parent.name;
    }


}
