﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : MonoBehaviour {

    public Transform target;
    public Transform spawnBox;


	public void Teleport() {
        target.position = spawnBox.position;
    } 
}
