﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorChange : ActivableComponent { 

    public SpriteRenderer sprite;
    public int index;
    public Color[] colors = new[] {Color.red, Color.yellow, Color.blue };

    public override void Switch() {
        index = (index + 1) % colors.Length;
        sprite.color = colors[index];
    }
}
