﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour {

    public GameObject flarePrefab;

    void OnCollisionStay(Collision collision) {
        
        for (int i = 0; i < collision.contacts.Length; i++) {
            var flare = Instantiate(flarePrefab);
            flare.transform.position = collision.contacts[i].point;
        }
            }
}
